﻿using System;
using TimerEventArgs.TimerEventArgs;

namespace TimerEventArgs
{
    /// <summary>
    /// A custom class for simulating a countdown clock, which implements the ability to send a messages and additional
    /// information about the Started, Tick and Stopped events to any types that are subscribing the specified events.
    /// 
    /// - When creating a CustomTimer object, it must be assigned:
    ///     - name (not null or empty string, otherwise ArgumentException will be thrown);
    ///     - the number of ticks (the number must be greater than 0 otherwise an exception will throw an ArgumentException).
    /// 
    /// - After the timer has been created, it should fire the Started event, the event should contain information about
    /// the name of the timer and the number of ticks to start.
    /// 
    /// - After starting the timer, it fires Tick events, which contain information about the name of the timer and
    /// the number of ticks left for triggering, there should be delays between Tick events, delays are modeled by Thread.Sleep.
    /// 
    /// - After all Tick events are triggered, the timer should start the Stopped event, the event should contain information about
    /// the name of the timer.
    /// </summary>
    public class Timer
    {
        private readonly string name;
        private int ticks;

        /// <summary>
        /// Initializes a new instance of the <see cref="Timer"/> class.
        /// </summary>
        /// <param name="name">Name of timer.</param>
        /// <param name="ticks">Ticks.</param>
        /// <exception cref="ArgumentException">Thrown when name is null or empty or then ticks are less or equal to 0.</exception>
        public Timer(string name, int ticks)
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentException("Name cannot be null or empty.");
            }

            if (ticks <= 0)
            {
                throw new ArgumentException("Ticks cannot be less or equal to 0.");
            }
            
            this.name = name;
            this.ticks = ticks;
        }
        
        /// <summary>
        /// Represents Start event.
        /// </summary>
        public event EventHandler<TimerStartEventArgs> Started;
        
        /// <summary>
        /// Represents Tick event.
        /// </summary>
        public event EventHandler<TimerTickEventArgs> Tick;
        
        /// <summary>
        /// Represents Stop event.
        /// </summary>
        public event EventHandler<TimerStopEventArgs> Stopped;

        /// <summary>
        /// Runs the timer.
        /// </summary>
        public void Run()
        {
            this.OnStarted();

            while (this.ticks != 1)
            {
                this.ticks--;
                this.OnTick();
            }
            
            this.OnStopped();
        }

        protected virtual void OnStarted()
        {
            this.Started?.Invoke(this, new TimerStartEventArgs(this.name, this.ticks));
        }

        protected virtual void OnTick()
        {
            this.Tick?.Invoke(this, new TimerTickEventArgs(this.name, this.ticks));
        }

        protected virtual void OnStopped()
        {
            this.Stopped?.Invoke(this, new TimerStopEventArgs(this.name));
        }
    }
}
