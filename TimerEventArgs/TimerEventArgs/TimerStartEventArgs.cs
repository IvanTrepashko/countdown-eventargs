﻿using System;

namespace TimerEventArgs.TimerEventArgs
{
    public class TimerStartEventArgs : EventArgs
    {
        public string Name { get; }
        
        public int Ticks { get; }

        public TimerStartEventArgs(string name, int ticks)
        {
            Name = name;
            Ticks = ticks;
        }
    }
}