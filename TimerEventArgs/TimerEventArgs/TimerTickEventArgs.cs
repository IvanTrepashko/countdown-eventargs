﻿using System;

namespace TimerEventArgs.TimerEventArgs
{
    public class TimerTickEventArgs : EventArgs
    {
        public string Name { get; }
        
        public int Ticks { get; }

        public TimerTickEventArgs(string name, int ticks)
        {
            this.Name = name;
            this.Ticks = ticks;
        }
    }
}