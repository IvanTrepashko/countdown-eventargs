﻿using System;

namespace TimerEventArgs.TimerEventArgs
{
    public class TimerStopEventArgs : EventArgs
    {
        public string Name { get; }

        public TimerStopEventArgs(string name)
        {
            Name = name;
        }
    }
}