﻿using System;
using TimerEventArgs.Interfaces;
using TimerEventArgs.TimerEventArgs;

namespace TimerEventArgs.Implementations
{
    /// <inheritdoc/>
    public class CountDownNotifier : ICountDownNotifier
    {
        private readonly Timer timer;

        public CountDownNotifier(Timer timer)
        {
            this.timer = timer ?? throw new ArgumentNullException(nameof(timer));
        }

        public void Init(EventHandler<TimerStartEventArgs> startHandler, EventHandler<TimerStopEventArgs> stopHandler, EventHandler<TimerTickEventArgs> tickHandler)
        {
            this.timer.Started += startHandler;
            this.timer.Tick += tickHandler;
            this.timer.Stopped += stopHandler;
        }

        /// <inheritdoc/>
        public void Run()
        {
            this.timer.Run();
        }
    }

}